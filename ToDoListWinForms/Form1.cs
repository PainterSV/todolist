﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ToDoListWinForms
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            RegisterPanel.Visible = false;
            LoginPanel.Visible = true;
        }

        private void SignUpButton_Click(object sender, EventArgs e)
        {
            LoginPanel.Visible = false;
            RegisterPanel.Visible = true;
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            try
            {
                var Client = new ToDoListService.TaskListServiceClient("BasicHttpBinding_ITaskListService");
                if (Client.Register(RegNameTextBox.Text, RegPassTextBox.Text, RegRepPassTextBox.Text))
                {
                    string UName = RegNameTextBox.Text;
                    ResultLabel.Text = "Ok";
                    Client.Close();
                    TaskForm TForm = new TaskForm(UName);
                    TForm.Show();
                    this.Hide();
                    
                }
                else
                {
                    ResultLabel.Text = "Such user name already exist, or the password has not been confirmed  correct";
                }
            }
            catch(SystemException)
            {
                ResultLabel.Text = "Could not connect to the server";
            }
            
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            try
            {

            var Client = new ToDoListService.TaskListServiceClient("BasicHttpBinding_ITaskListService");
                if (Client.LogIn(LogNameTextBox.Text, LogPassTextBox.Text))
                {
                    string UName = LogNameTextBox.Text;
                    ResultLabel.Text = "Ok";
                    TaskForm TForm = new TaskForm(UName);
                    TForm.Show();
                    this.Hide();
                    Client.Close();
                }
                else
                    ResultLabel.Text = "Don't  a valid user name or password";
            }
            catch (SystemException)
            {
                ResultLabel.Text = "Could not connect to the server";
            }
        }
    }
}
