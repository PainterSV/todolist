﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskListWcfService;

namespace ToDoListWinForms
{
    public partial class TaskForm : Form
    {
        private int TaskIDforDeleting;
        private string LogUserName;
        private string RegUserName;
        private bool CloseOrOut = false;
        private int RowIndex = 0;
        private ToDoListService.TaskListServiceClient Client = new ToDoListService.TaskListServiceClient("BasicHttpBinding_ITaskListService");

        public TaskForm(string Name)
        {
            InitializeComponent();
            {
                LogUserName = Name;
                var Client = new ToDoListService.TaskListServiceClient("BasicHttpBinding_ITaskListService");
                var UserTasks = Client.GetTask(LogUserName);


                foreach(var TaskCell in UserTasks)
                {
                    TaskDataGrid.Rows.Add(TaskCell.MarkTask, TaskCell.DateForTask.ToString().Remove(10), TaskCell.TimeForTask, TaskCell.Task, TaskCell.TaskID);
                }    
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseOrOut = true;
            Program.Context.MainForm.Close();
            Client.LogOut(LogUserName);
            Client.Close();
            this.Close();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseOrOut = true;
            Program.Context.MainForm.Show();
            Client.LogOut(LogUserName);
            Client.Close();
            this.Close();  
        }

        private void TaskForm_Load(object sender, EventArgs e)
        {  
        }

        private void TaskForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if((e.CloseReason == CloseReason.UserClosing ) && !CloseOrOut)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            int RowIndex = TaskDataGrid.SelectedCells[0].RowIndex;
            var Client = new ToDoListService.TaskListServiceClient("BasicHttpBinding_ITaskListService");
            Client.DeleteTask(TaskIDforDeleting);
            TaskDataGrid.Rows.RemoveAt(RowIndex);
        }

        private void TaskDataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                RowIndex = TaskDataGrid.SelectedCells[0].RowIndex;
                var ch = TaskDataGrid.Rows[RowIndex].Cells[0].Value;
                StatusCheckBox.Checked = Boolean.Parse(ch.ToString());
                dateTimePicker.Value = System.DateTime.Parse(TaskDataGrid.Rows[RowIndex].Cells[1].Value.ToString());
                TimeTextBox.Text = TaskDataGrid.Rows[RowIndex].Cells[2].Value.ToString();
                TaskTextBox.Text = TaskDataGrid.Rows[RowIndex].Cells[3].Value.ToString();
                TaskIDforDeleting = (int)TaskDataGrid.Rows[RowIndex].Cells[4].Value;
            }
            catch (NullReferenceException) { }
        }

        private void SetButton_Click(object sender, EventArgs e)
        {
            TaskDataGrid.Rows[RowIndex].Cells[0].Value = StatusCheckBox.Checked;
            TaskDataGrid.Rows[RowIndex].Cells[1].Value = dateTimePicker.Value.ToShortDateString();
            TaskDataGrid.Rows[RowIndex].Cells[2].Value = TimeTextBox.Text;
            TaskDataGrid.Rows[RowIndex].Cells[3].Value = TaskTextBox.Text;

            var Client = new ToDoListService.TaskListServiceClient("BasicHttpBinding_ITaskListService");
            Client.SetTask(TaskIDforDeleting, StatusCheckBox.Checked, dateTimePicker.Value, TimeTextBox.Text, TaskTextBox.Text);    
        }

        private void TimeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {/*
            
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
            
            if (TimeTextBox.Text.Count() == 1)
            {
                TimeTextBox.Text += ":";
            }*/
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            TaskDataGrid.Rows.Add(StatusCheckBox.Checked, dateTimePicker.Value.ToShortDateString(), TimeTextBox.Text, TaskTextBox.Text); 
            Client.AddTask(LogUserName, StatusCheckBox.Checked, dateTimePicker.Value, TimeTextBox.Text, TaskTextBox.Text);
        }
  
    }
}
