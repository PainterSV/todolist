﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToDoListWinForms.ToDoListService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ToDoListService.ITaskListService")]
    public interface ITaskListService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/LogIn", ReplyAction="http://tempuri.org/ITaskListService/LogInResponse")]
        bool LogIn(string UserName, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/LogIn", ReplyAction="http://tempuri.org/ITaskListService/LogInResponse")]
        System.Threading.Tasks.Task<bool> LogInAsync(string UserName, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/LogOut", ReplyAction="http://tempuri.org/ITaskListService/LogOutResponse")]
        void LogOut(string LogUserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/LogOut", ReplyAction="http://tempuri.org/ITaskListService/LogOutResponse")]
        System.Threading.Tasks.Task LogOutAsync(string LogUserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/Register", ReplyAction="http://tempuri.org/ITaskListService/RegisterResponse")]
        bool Register(string UserName, string Password, string CheckPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/Register", ReplyAction="http://tempuri.org/ITaskListService/RegisterResponse")]
        System.Threading.Tasks.Task<bool> RegisterAsync(string UserName, string Password, string CheckPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/GetTask", ReplyAction="http://tempuri.org/ITaskListService/GetTaskResponse")]
        TaskListWcfService.UserTask[] GetTask(string UserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/GetTask", ReplyAction="http://tempuri.org/ITaskListService/GetTaskResponse")]
        System.Threading.Tasks.Task<TaskListWcfService.UserTask[]> GetTaskAsync(string UserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/AddTask", ReplyAction="http://tempuri.org/ITaskListService/AddTaskResponse")]
        void AddTask(string UserName, bool Status, System.DateTime Data, string Time, string Task);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/AddTask", ReplyAction="http://tempuri.org/ITaskListService/AddTaskResponse")]
        System.Threading.Tasks.Task AddTaskAsync(string UserName, bool Status, System.DateTime Data, string Time, string Task);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/SetTask", ReplyAction="http://tempuri.org/ITaskListService/SetTaskResponse")]
        void SetTask(int TaskID, bool Status, System.DateTime Data, string Time, string Task);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/SetTask", ReplyAction="http://tempuri.org/ITaskListService/SetTaskResponse")]
        System.Threading.Tasks.Task SetTaskAsync(int TaskID, bool Status, System.DateTime Data, string Time, string Task);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/DeleteTask", ReplyAction="http://tempuri.org/ITaskListService/DeleteTaskResponse")]
        void DeleteTask(int TaskID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITaskListService/DeleteTask", ReplyAction="http://tempuri.org/ITaskListService/DeleteTaskResponse")]
        System.Threading.Tasks.Task DeleteTaskAsync(int TaskID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ITaskListServiceChannel : ToDoListWinForms.ToDoListService.ITaskListService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TaskListServiceClient : System.ServiceModel.ClientBase<ToDoListWinForms.ToDoListService.ITaskListService>, ToDoListWinForms.ToDoListService.ITaskListService {
        
        public TaskListServiceClient() {
        }
        
        public TaskListServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TaskListServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TaskListServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TaskListServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool LogIn(string UserName, string Password) {
            return base.Channel.LogIn(UserName, Password);
        }
        
        public System.Threading.Tasks.Task<bool> LogInAsync(string UserName, string Password) {
            return base.Channel.LogInAsync(UserName, Password);
        }
        
        public void LogOut(string LogUserName) {
            base.Channel.LogOut(LogUserName);
        }
        
        public System.Threading.Tasks.Task LogOutAsync(string LogUserName) {
            return base.Channel.LogOutAsync(LogUserName);
        }
        
        public bool Register(string UserName, string Password, string CheckPassword) {
            return base.Channel.Register(UserName, Password, CheckPassword);
        }
        
        public System.Threading.Tasks.Task<bool> RegisterAsync(string UserName, string Password, string CheckPassword) {
            return base.Channel.RegisterAsync(UserName, Password, CheckPassword);
        }
        
        public TaskListWcfService.UserTask[] GetTask(string UserName) {
            return base.Channel.GetTask(UserName);
        }
        
        public System.Threading.Tasks.Task<TaskListWcfService.UserTask[]> GetTaskAsync(string UserName) {
            return base.Channel.GetTaskAsync(UserName);
        }
        
        public void AddTask(string UserName, bool Status, System.DateTime Data, string Time, string Task) {
            base.Channel.AddTask(UserName, Status, Data, Time, Task);
        }
        
        public System.Threading.Tasks.Task AddTaskAsync(string UserName, bool Status, System.DateTime Data, string Time, string Task) {
            return base.Channel.AddTaskAsync(UserName, Status, Data, Time, Task);
        }
        
        public void SetTask(int TaskID, bool Status, System.DateTime Data, string Time, string Task) {
            base.Channel.SetTask(TaskID, Status, Data, Time, Task);
        }
        
        public System.Threading.Tasks.Task SetTaskAsync(int TaskID, bool Status, System.DateTime Data, string Time, string Task) {
            return base.Channel.SetTaskAsync(TaskID, Status, Data, Time, Task);
        }
        
        public void DeleteTask(int TaskID) {
            base.Channel.DeleteTask(TaskID);
        }
        
        public System.Threading.Tasks.Task DeleteTaskAsync(int TaskID) {
            return base.Channel.DeleteTaskAsync(TaskID);
        }
    }
}
