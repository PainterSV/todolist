﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;

namespace TaskListWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TaskListService" in both code and config file together.
    public class TaskListService : ITaskListService
    {
        
        public bool LogIn(string LogUserName, string Password)
        {
            bool Result = false;
            var DB = new LINQtoSQLDataContext();

            foreach (var Users in DB.Users)
            {
                if ( GetMD5Hash(Password) == Users.USerPassword && LogUserName == Users.UserName)
                {
                    Result = true;
                    Console.WriteLine("User {0} connected", LogUserName);
                    break;
                }
            }
            DB.Connection.Close();
            return Result;
        }

        public void LogOut(string LogUserName)
        {
            Console.WriteLine("User {0} disconnected", LogUserName);
        }

        public bool Register(string RegUserName, string Password, string CheckPassword)
        {
            bool Result = false;
            bool ResultForName = false;
            bool ResultForPass = false;

            var DB = new LINQtoSQLDataContext();
            var UserNamesDB = from Users in DB.Users select Users.UserName;

            

            foreach (var Users in DB.Users)
            {
                if (Users.UserName == RegUserName)
                {
                    ResultForName = false;
                    break;
                }
                else
                {
                    ResultForName = true;
                }
                
            }
            if(Password == CheckPassword)
            {
                ResultForPass = true;
            }
            if(ResultForPass && ResultForName)
            {
                Result = true;
                User NewUser = new User();

                NewUser.UserName = RegUserName;
                NewUser.USerPassword = GetMD5Hash(Password);
                DB.Users.InsertOnSubmit(NewUser);

                Console.WriteLine("New user {0} was registered", RegUserName);
            }
            DB.SubmitChanges();
            DB.Connection.Close();
            return Result;
        }


        public IQueryable<UserTask> GetTask(string LogUserName)
        {
            
            int IDint = 0;
            var DB = new LINQtoSQLDataContext();
            var UserIDDB = (from Users in DB.Users where Users.UserName == LogUserName select Users.UserID);
   
            foreach (var ID in UserIDDB)
            {
                IDint = ID;
            }
            
            var UserTaskDBRes = from UsersTask in DB.UserTasks where UsersTask.UserID == IDint select UsersTask;   
            DB.Connection.Close();

            return UserTaskDBRes;
        }
        public void AddTask(string UserName, bool Status, System.DateTime Data, string Time, string Task)
        {
         
            int IDint = 0;
            var DB = new LINQtoSQLDataContext();
            
            var UserID = (from Users in DB.Users where Users.UserName == UserName select Users.UserID);
            foreach (var ID in UserID)
            {
                IDint = ID;
            }
            var UserTaskDBRes = from UsersTask in DB.UserTasks where UsersTask.UserID == IDint select UsersTask;
            
            UserTask UTask = new UserTask();
            UTask.UserID = IDint;
            UTask.MarkTask = Status;
            UTask.DateForTask = Data;
            UTask.TimeForTask = Time;
            UTask.Task = Task;
            DB.UserTasks.InsertOnSubmit(UTask);
            DB.SubmitChanges();
            DB.Connection.Close();
        }
        public void DeleteTask(int TaskID)
        {
            var DB = new LINQtoSQLDataContext();
            var DeleteUserTaskDBRes = from UsersTask in DB.UserTasks where UsersTask.TaskID == TaskID  select UsersTask;

            if (DeleteUserTaskDBRes.Count() > 0)
            {
                DB.UserTasks.DeleteOnSubmit(DeleteUserTaskDBRes.First());
                DB.SubmitChanges();
            }
            DB.Connection.Close();
        }

        public void SetTask(int TaskID, bool Status, System.DateTime Data, string Time, string Task)
        {
            var DB = new LINQtoSQLDataContext();
            var SetUserTaskDBRes = from UsersTask in DB.UserTasks where UsersTask.TaskID == TaskID select UsersTask;

            foreach (var UTask in SetUserTaskDBRes)
            {
                UTask.MarkTask = Status;
                UTask.DateForTask = Data;
                UTask.TimeForTask = Time;
                UTask.Task = Task;
            }
            DB.SubmitChanges();
            DB.Connection.Close();
        }

        private string GetMD5Hash(string Pass)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] CheckSum = md5.ComputeHash(Encoding.UTF8.GetBytes(Pass));
            string Result = BitConverter.ToString(CheckSum).Replace("-", String.Empty);
            return Result;
        }
        
    }
}
