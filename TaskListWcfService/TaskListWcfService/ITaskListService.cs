﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TaskListWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITaskListService" in both code and config file together.
    [ServiceContract]
    public interface ITaskListService
    {
        [OperationContract]
        bool LogIn(string UserName, string Password);
        [OperationContract]
        void LogOut(string LogUserName);
        [OperationContract]
        bool Register(string UserName, string Password, string CheckPassword);



        [OperationContract]
        IQueryable<UserTask> GetTask(string UserName);
        [OperationContract]
        void AddTask(string UserName, bool Status, System.DateTime Data, string Time, string Task);
        [OperationContract]
        void SetTask(int TaskID, bool Status, System.DateTime Data, string Time, string Task);
        [OperationContract]
        void DeleteTask(int TaskID);
       
    }
}
